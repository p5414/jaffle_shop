
  create or replace  view JAFFLE_SHOP."HE"."stg_orders" 
  
   as (
    with source as (
    select * from JAFFLE_SHOP."HE"."raw_orders"

),

renamed as (

    select
        id as order_id,
        user_id as customer_id,
        order_date,
        status

    from source

)

select * from renamed
  );
